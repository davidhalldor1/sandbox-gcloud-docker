FROM alpine:3.7 AS builder
LABEL maintainer="Davíð Halldór Lúðvíksson"
RUN apk add --no-cache \
    python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
# We copy just the requirements.txt first to leverage Docker cache
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
#CMD ["gunicorn", "main:app", "-b", "0.0.0.0:5000"]

FROM nginx:stable
# main configuration file
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/web /usr/share/nginx/html